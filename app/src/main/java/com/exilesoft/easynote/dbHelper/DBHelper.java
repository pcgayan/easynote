package com.exilesoft.easynote.dbHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.exilesoft.easynote.domain.Note;

import java.util.Date;

/**
 * Created by gpr on 12/9/2014.
 */
public class DBHelper extends SQLiteOpenHelper {
    private final String TAG = SQLiteOpenHelper.class.getSimpleName();
    public static final int DATABASE_VERSION = 8;
    public static final String DATABASE_NAME = "notes.db";
    public static final String TEXT_TYPE = " TEXT";
    public static final String TIMESTAMP_TYPE = " TIMESTAMP";
    public static final String INTEGER_PRIMARY_KEY = " INTEGER PRIMARY KEY";
    public static final String COMMA_SEP = ",";
    private static NoteDBHelper noteDBHelper;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        noteDBHelper = new NoteDBHelper(this);
    }

    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "Creating DB....");
        db.execSQL(NoteDBHelper.SQL_CREATE_ENTRIES);
        insertInitialData(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(TAG, "Upgrading DB from " + oldVersion + " to " + newVersion);
        db.execSQL(NoteDBHelper.SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void insertInitialData(SQLiteDatabase db) {
        Log.d(TAG, "inserting initial Data....");
        Date date = new Date();
        Note note = new Note("Hello there!", "This is the first note :)",
                date.getTime(), date.getTime());
        noteDBHelper.create(db, note);
    }

    public void cleanDB(SQLiteDatabase db) {
        Log.d(TAG, "cleaning DB....");
    }

    public NoteDBHelper getNoteDBHelper() {
        return noteDBHelper;
    }
}
