package com.exilesoft.easynote.dbHelper;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.exilesoft.easynote.domain.Note;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by gpr on 12/5/2014.
 */
public class NoteDBHelper {
    private static final String TAG = NoteDBHelper.class.getSimpleName();
    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + Note.TABLE_NAME + " (" +
                    Note._ID + DBHelper.INTEGER_PRIMARY_KEY + DBHelper.COMMA_SEP +
                    Note.TITLE + DBHelper.TEXT_TYPE + DBHelper.COMMA_SEP +
                    Note.CONTENT + DBHelper.TEXT_TYPE + DBHelper.COMMA_SEP +
                    Note.CREATED_TIMESTAMP + DBHelper.TIMESTAMP_TYPE + DBHelper.COMMA_SEP +
                    Note.MODIFIED_TIMESTAMP + DBHelper.TIMESTAMP_TYPE + " )";
    public static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + Note.TABLE_NAME;
    private SQLiteOpenHelper context;

    public NoteDBHelper(SQLiteOpenHelper context) {
        this.context = context;
    }

    public long create(Note note) {
        Log.d(TAG, "inserting sample notes....");
        SQLiteDatabase db = context.getWritableDatabase();
        return create(db, note);
   }

    public long create(SQLiteDatabase db, Note note) {
        Log.d(TAG, "inserting Data....");

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(Note.TITLE, note.getTitle());
        values.put(Note.CONTENT, note.getContent());
        values.put(Note.CREATED_TIMESTAMP, note.getCreatedTimestamp());
        values.put(Note.MODIFIED_TIMESTAMP, note.getModifiedTimestamp());

        return db.insert(
                Note.TABLE_NAME,
                Note._ID,
                values);
    }

    public Cursor readData(String filter, String[] value){
        Log.d(TAG, "loading notes to memory....");
        SQLiteDatabase db = context.getReadableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                Note._ID,
                Note.TITLE,
                Note.CONTENT
          };

        // How you want the results sorted in the resulting Cursor
        String sortOrder = Note._ID + " DESC";

        return db.query(
                Note.TABLE_NAME,                          // The table to query
                projection,                               // The columns to return
                filter,                                   // The columns for the WHERE clause
                value,                                    // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );
    }

    public Map<Integer, Note> getAllNotes() {
        Map<Integer, Note> notesMap = new HashMap<Integer, Note>();
        Cursor cursor = readData(null, new String[0]);
        cursor.moveToFirst();

        while(true) {
            if (cursor.isAfterLast()) {
                cursor.close();
                break;
            }
            Note note = getNote(cursor);
            notesMap.put(note.getId(), note);
            cursor.moveToNext();
        }
        cursor.close();
        return notesMap;
    }

    public List<Note> getAllNoteInfo() {
        List<Note> noteList = new ArrayList<Note>();
        Cursor cursor = readData(null, new String[0]);
        cursor.moveToFirst();

        while(true) {
            if (cursor.isAfterLast()) {
                break;
            }
            noteList.add(getNote(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return noteList;
    }

    private Note getNote(Cursor cursor) {
        int noteId = cursor.getInt(
                cursor.getColumnIndexOrThrow(Note._ID)
        );
        String noteTitle = cursor.getString(
                cursor.getColumnIndexOrThrow(Note.TITLE)
        );
        String noteContent = cursor.getString(
                cursor.getColumnIndexOrThrow(Note.CONTENT)
        );
        return new Note(noteId, noteTitle, noteContent);
    }

    public Note getNote(int noteId) {
        String selectQuery = "SELECT  * FROM " + Note.TABLE_NAME + " WHERE " +
                Note._ID + " = " + noteId;
        SQLiteDatabase db = context.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        Note note = null;
        if (!cursor.isAfterLast()) {
            note = getNote(cursor);
        }
        cursor.close();
        return note;
    }

    public int delete(int noteId) {
        SQLiteDatabase db = context.getReadableDatabase();
        return db.delete(Note.TABLE_NAME, Note._ID + " = " + noteId, null);
    }
}
