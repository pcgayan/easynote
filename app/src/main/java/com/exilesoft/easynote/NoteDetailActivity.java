package com.exilesoft.easynote;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.exilesoft.easynote.dbHelper.DBHelper;


public class NoteDetailActivity extends ActionBarActivity implements NoteDetailFragment.OnFragmentInteractionListener {
    private final static String TAG = NoteDetailActivity.class.getSimpleName();
    public static final String NOTE_ID = "noteId";
    private DBHelper dBHelper;
    private int noteId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_detail);
        final Bundle bundle = getIntent().getExtras();
        noteId = bundle.getInt(NOTE_ID);
        Log.d(TAG, "onCreate for "+ noteId);
        dBHelper = new DBHelper(this);

        if(savedInstanceState == null){
            NoteDetailFragment noteDetailFragment = NoteDetailFragment.newInstance(noteId);
            getFragmentManager().beginTransaction().add(R.id.noteDetailContainer, noteDetailFragment).commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_note_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
             case R.id.action_delete:
                Log.d(TAG, "action delete");
                dBHelper.getNoteDBHelper().delete(noteId);
                setResult(RESULT_OK);
                finish();
                return  true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onFragmentInteraction(int action) {
        Log.d(TAG, "Even firing on note detail " + action);
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
    }
}
