package com.exilesoft.easynote;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.exilesoft.easynote.dbHelper.DBHelper;
import com.exilesoft.easynote.domain.Note;

import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link NoteDetailFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NoteDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class NoteDetailFragment extends Fragment {
    private final static String TAG = NoteDetailFragment.class.getSimpleName();
    private int mNoteId;
    private OnFragmentInteractionListener mListener;
    private DBHelper dBHelper;

    public static NoteDetailFragment newInstance(int noteId) {
        NoteDetailFragment fragment = new NoteDetailFragment();
        Bundle args = new Bundle();
        args.putInt(Note._ID, noteId);
        fragment.setArguments(args);
        return fragment;
    }

    public NoteDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        if (getArguments() != null) {
             mNoteId = getArguments().getInt(Note._ID);
        }
        Log.d(TAG, "onCreate for " + mNoteId);
        dBHelper = new DBHelper(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        Note note = dBHelper.getNoteDBHelper().getNote(mNoteId);

        View view = inflater.inflate(R.layout.fragment_note_detail, container, false);

        TextView noteTitle = (TextView) view.findViewById(R.id.noteTitle);
        noteTitle.append(note.getTitle());

        TextView noteContent = (TextView) view.findViewById(R.id.noteContent);
        noteContent.append(note.getContent());

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_note_detail_fragment, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       /* switch (item.getItemId()) {
             case R.id.action_delete:
                Log.d(TAG, "action delete");
                dBHelper.getNoteDBHelper().delete(mNoteId);
                mListener.onFragmentInteraction(getActivity().RESULT_OK);
                return  true;
            default:
                return super.onOptionsItemSelected(item);
        }*/
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        Log.d(TAG, "onDetach");
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(int noteId);
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
    }
}
