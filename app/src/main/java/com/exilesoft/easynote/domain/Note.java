package com.exilesoft.easynote.domain;

import android.provider.BaseColumns;

import com.exilesoft.easynote.dbHelper.NoteDBHelper;

import java.security.Timestamp;

/**
 * Created by gpr on 12/5/2014.
 */
public class Note implements BaseColumns{
    public static final String TABLE_NAME = "notes";
    public static final String TITLE = "title";
    public static final String CONTENT = "content";
    public static final String CREATED_TIMESTAMP = "createdTimestamp";
    public static final String MODIFIED_TIMESTAMP = "modifiedTimestamp";

    private int id;
    private String title;
    private String content;
    private long createdTimestamp;
    private long modifiedTimestamp;

    public Note(String title, String content, long createdTimestamp, long modifiedTimestamp) {
        this.title = title;
        this.content = content;
        this.createdTimestamp = createdTimestamp;
        this.modifiedTimestamp = modifiedTimestamp;
    }

    public Note(int id, String title, String content) {
        this.id = id;
        this.title = title;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    @Override
    public String toString() {
        return title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getCreatedTimestamp() {
        return createdTimestamp;
    }

    public long getModifiedTimestamp() {
        return modifiedTimestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Note)) return false;

        Note note = (Note) o;

        if (createdTimestamp != note.createdTimestamp) return false;
        if (id != note.id) return false;
        if (modifiedTimestamp != note.modifiedTimestamp) return false;
        if (!content.equals(note.content)) return false;
        if (!title.equals(note.title)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + title.hashCode();
        result = 31 * result + content.hashCode();
        result = 31 * result + (int) (createdTimestamp ^ (createdTimestamp >>> 32));
        result = 31 * result + (int) (modifiedTimestamp ^ (modifiedTimestamp >>> 32));
        return result;
    }
}
