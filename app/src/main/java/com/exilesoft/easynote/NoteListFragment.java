package com.exilesoft.easynote;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;


import com.exilesoft.easynote.dbHelper.DBHelper;
import com.exilesoft.easynote.domain.Note;

import java.util.ArrayList;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnFragmentInteractionListener}
 * interface.
 * @author Gayan Priyanatha
 */
public class NoteListFragment extends Fragment implements AbsListView.OnItemClickListener {
    private final static String TAG = NoteListFragment.class.getSimpleName();
    private OnFragmentInteractionListener mListener;
    private AbsListView mListView;
    private ListAdapter mAdapter;
    private DBHelper dBHelper;
    private final List<Note> notes=new ArrayList<>();

    public static NoteListFragment newInstance() {
        return new NoteListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
        //mAdapter = new ArrayAdapter<DummyContent.DummyItem>(getActivity(),
               // android.R.layout.simple_list_item_1, android.R.id.text1, DummyContent.ITEMS);
        dBHelper = new DBHelper(getActivity());
        //if (savedInstanceState == null) {
            mAdapter = new ArrayAdapter<Note>(getActivity(),
                    android.R.layout.simple_list_item_1, android.R.id.text1, notes);
        //}
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View view = inflater.inflate(R.layout.fragment_notelist, container, false);
        mListView = (AbsListView) view.findViewById(android.R.id.list);
        mListView.setOnItemClickListener(this);
        mListView.setAdapter(mAdapter);
        refreshNoteList();
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        Log.d(TAG, "onAttach");
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_note_list_fragment, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /*switch (item.getItemId()) {
            case R.id.action_search:
                Log.d(TAG, "action search");
                return true;
            case R.id.action_add:
                Log.d(TAG, "action add");
                // TODO : dummy code
                Date date = new Date();
                Note note = new Note(Long.toString(date.getTime()), date.toString(),
                        date.getTime(), date.getTime());
                note.setId((int) dBHelper.getNoteDBHelper().create(note));
                mListener.onFragmentInteraction(note.getId());
                return  true;
            default:
                return super.onOptionsItemSelected(item);
        }*/
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d(TAG, "onItemClick " + id);
        if (null != mListener) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            //mListener.onFragmentInteraction(DummyContent.ITEMS.get(position).id);
            Note note = (Note) mAdapter.getItem(position);
            mListener.onFragmentInteraction(note.getId());
        }
    }

    public void setEmptyText(CharSequence emptyText) {
        View emptyView = mListView.getEmptyView();

        if (emptyView instanceof TextView) {
            ((TextView) emptyView).setText(emptyText);
        }
    }

    public void refreshNoteList() {
        notes.clear();
        notes.addAll(dBHelper.getNoteDBHelper().getAllNoteInfo());
        ((ArrayAdapter)mAdapter).notifyDataSetChanged();
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(int noteId);
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();

    }

    @Override
    public void onDetach() {
        Log.d(TAG, "onDetach");
        super.onDetach();
        mListener = null;
    }
}
