package com.exilesoft.easynote;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.exilesoft.easynote.dbHelper.DBHelper;
import com.exilesoft.easynote.domain.Note;

import java.util.Date;

/**
 * Main Activity on easy note app
 * http://stackoverflow.com/questions/26606527/android-refresh-a-fragment-list-from-its-parent-activity
 * http://stackoverflow.com/questions/19677218/refresh-a-fragment-from-its-parent-activity
 * @author Gayan Priyanatha
 */
public class NoteListActivity extends ActionBarActivity implements NoteListFragment.OnFragmentInteractionListener{
    private final static String TAG = NoteListActivity.class.getSimpleName();
    public static final String NOTE_LIST = "note_list";
    private DBHelper dBHelper;
    private NoteListFragment noteListFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_list);
        dBHelper = new DBHelper(this);

        if(savedInstanceState == null){
            noteListFragment = NoteListFragment.newInstance();
            getFragmentManager().beginTransaction().add(R.id.nodeListContainer, noteListFragment,NOTE_LIST).commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_note_list, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                Log.d(TAG, "action search");
                return true;
            case R.id.action_add:
                Log.d(TAG, "action add");
                // TODO : dummy code
                Date date = new Date();
                Note note = new Note(Long.toString(date.getTime()), date.toString(),
                        date.getTime(), date.getTime());
                note.setId((int) dBHelper.getNoteDBHelper().create(note));
                Intent intent = new Intent(this, NoteDetailActivity.class);
                intent.putExtra(NoteDetailActivity.NOTE_ID, note.getId());
                startActivityForResult(intent, 1);
                setResult(RESULT_OK);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onFragmentInteraction(int noteId) {
        Log.d(TAG, "Even firing on note detail " + noteId);
        Intent intent = new Intent(this, NoteDetailActivity.class);
        intent.putExtra(NoteDetailActivity.NOTE_ID, noteId);
        startActivityForResult(intent, 1);
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult");
        if (requestCode > 0) {
             if (resultCode == RESULT_OK) {
                ((NoteListFragment) getFragmentManager().findFragmentByTag(NOTE_LIST)).
                        refreshNoteList();
            }

           //getFragmentManager().beginTransaction().detach(noteListFragment).commit();
           //noteListFragment = NoteListFragment.newInstance();
           //getFragmentManager().beginTransaction().add(R.id.nodeListContainer, noteListFragment,"note_list").commit();
        }
    }
}
